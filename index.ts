var app = require('express')();
var http = require('http').createServer(app);
var io = require('socket.io')(http);

// var whatever = goes here;
var playerCount = 0;

//this is bad code.....

app.get('/', (req, res) => {
    res.sendFile(__dirname + '/button-viewer.html');
});

app.get('/button-viewer.html', (req, res) => {
    res.sendFile(__dirname + '/button-viewer.html');
});

app.get('/button-viewer', (req, res) => {
    res.sendFile(__dirname + '/button-viewer.html');
});

app.get('/game', (req, res) => {
    res.sendFile(__dirname + '/button-viewer.html');
});

app.get('/button-pressable.html', (req, res) => {
    res.sendFile(__dirname + '/button-pressable.html');
});

app.get('/press', (req, res) => {
    res.sendFile(__dirname + '/button-pressable.html');
});

app.get('/button-pressable', (req, res) => {
    res.sendFile(__dirname + '/button-pressable.html');
});

app.get('/button.png', (req, res) => {
    res.sendFile(__dirname + '/button.png');
});

app.get('/button-pressed.png', (req, res) => {
    res.sendFile(__dirname + '/button-pressed.png');
});

app.get('/title.jpg', (req, res) => {
    res.sendFile(__dirname + '/title.jpg');
});


app.get('/buttonPress1.mp3', (req, res) => {
    res.sendFile(__dirname + '/buttonPress1.mp3');
});


io.on('connection', (socket) => {
    playerCount++;
    console.log('A player connected.\t Current number of connections: ' + playerCount);

    socket.on('disconnect', () => {
        playerCount--;

        console.log('A player disconnected.\t Current number of connections: ' + playerCount);
    });

    socket.on('reset button', (msg) => {
        console.log('reset button request recived');
        io.emit('reset', true);
    });

    socket.on('press button', (msg) => {
        console.log('button press request recived');
        io.emit('button pressed', true);
    });
});

http.listen(3000, () => {
  console.log('listening on *:3000');
});
